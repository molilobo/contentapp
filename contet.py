#!/usr/bin/env python3

import webapp
class ContentApp(webapp.webApp):# Hereda de la clase webApp

    def __init__(self, hostname, port):# Inicializo el objeto
        self.contents = {'/': "<p>opciones : hello , bye , error</p>",
                         '/hello': "<p>Hello world</p>",
                         '/bye': "<p>Bye world</p>",
                         '/error': "<p>Error page</p>",
                         }
        super().__init__(hostname, port)# Inicializo la clase padre

    def parse(self, request):# Parseo la petición
        # Tras dividir la cadena selecciono el segundo elemento de la lista
        return request.split(' ', 2)[1]

    def process(self, resource):# Proceso la petición

        if resource in self.contents:
            content = self.contents[resource]
            html = "<html><body><h1>" + content + "</h1></body></html>"
            code = "200 OK"

        else:# Si no encuentra el recurso salta el recurso error
            content = self.contents["/error"]
            html = "<html><body><h1>" + content + "</h1></body></html>"
            code = "404 Resource Not Found"
        return code, html


if __name__ == "__main__":
    webApp = ContentApp("localhost", 1234)
