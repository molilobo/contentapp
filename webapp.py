#!/usr/bin/env python3

import socket

class webApp:  # Clase principal de la aplicación web

    def parse(self, request):  # Se encarga de parsear la petición
        print("Parse: Nada que parsear")
        return None

    def process(self, parsedRequest):  # Se encarga de procesar la petición
        print("Process: Returning 200 OK")
        return "200 OK", "<html><body><h1>Hola Mundo </h1></body></html>"

    def __init__ (self, hostname, port):  # Se encarga de crear el socket y de escuchar peticiones
        # Crea un socket y se queda escuchando peticiones
        mySocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        mySocket.bind((hostname, port))

        # Admite un máximo de 5 conexiones pendientes
        mySocket.listen(5)

        while True:
            print("Waiting for connections")
            (recvSocket, address) = mySocket.accept()
            print("HTTP request received (going to parse and process):")
            request = recvSocket.recv(2048)
            print(request)
            parsedRequest = self.parse(request.decode('utf8'))
            (returnCode, htmlAnswer) = self.process(parsedRequest)
            print("Answering back...")
            response = "HTTP/1.1 " + returnCode + " \r\n\r\n" \
                       + htmlAnswer + "\r\n"
            recvSocket.send(response.encode('utf8'))
            recvSocket.close()


if __name__ == "__main__":
   WebApp12 = webApp("localhost", 1234)
